//a function that tunrs "processing" text on or off
jQuery.fn.dataTableExt.oApi.fnProcessingIndicator = function ( oSettings, onoff ) {
    if ( typeof( onoff ) == 'undefined' ) {
        onoff = true;
    }
    this.oApi._fnProcessingDisplay( oSettings, onoff );
};

//sets a filtering delay on keystrokes so not too many unnecesary searches are performed
//Authors: Zygimantas Berziunas, Allan Jardine and vex
jQuery.fn.dataTableExt.oApi.fnSetFilteringDelay = function ( oSettings, iDelay ) {
    var _that = this;
 
    if ( iDelay === undefined ) {
        iDelay = 250;
    }
      
    this.each( function ( i ) {
        $.fn.dataTableExt.iApiIndex = i;
        var
            $this = this,
            oTimerId = null,
            sPreviousSearch = null,
            anControl = $( 'input', _that.fnSettings().aanFeatures.f );
          
            anControl.unbind( 'keyup' ).bind( 'keyup', function() {
            var $$this = $this;
  
            if (sPreviousSearch === null || sPreviousSearch != anControl.val()) {
                window.clearTimeout(oTimerId);
                sPreviousSearch = anControl.val(); 
                oTimerId = window.setTimeout(function() {
                    $.fn.dataTableExt.iApiIndex = i;
                    _that.fnFilter( anControl.val() );
                }, iDelay);
            }
        });
          
        return this;
    } );
    return this;
};

//Language object for Datatables
//Translations are taken from the database
var dataTables_lang = {
    "sProcessing":	"{$DATATABLES_SPROCESSING}",
    "sLengthMenu":	"{$DATATABLES_SLENGTHMENU}",
    "sZeroRecords":	"{$DATATABLES_SZERORECORDS}",
    "sInfo":		"{$DATATABLES_SINFO}",
    "sInfoEmpty":    "{$DATATABLES_SINFOEMPTY}",
    "sInfoFiltered":	"{$DATATABLES_SINFOFILTERED}",
    "sInfoPostFix":	"{$DATATABLES_SINFOPOSTFIX}",
    "sSearch":       "{$DATATABLES_SSEARCH}",
    "sUrl":          "{$DATATABLES_SURL}",
    "oPaginate": {
        "sFirst":    "{$DATATABLES_OPAGINATE_SFIRST}",
        "sPrevious": "{$DATATABLES_OPAGINATE_SPREVIOUS}",
        "sNext":     "{$DATATABLES_OPAGINATE_SNEXT}",
        "sLast":     "{$DATATABLES_OPAGINATE_SLAST}"
    }
}