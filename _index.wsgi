import sys;
import os;
import traceback;
from StringIO import StringIO

os.chdir(os.path.dirname(__file__));
sys.path.append(os.getcwd());
sys.path.append(os.getcwd()+'/system');

import jupp;
from System import System;
from Status import Status;
from Header import Header; 

def dump_file(environ):
    f = file('z_input.txt', 'wb');
    aux = environ['wsgi.input'].read();
    f.write(aux);
    f.close();
    del aux;
    
    del environ['wsgi.file_wrapper'];
    del environ['wsgi.errors'];
    del environ['wsgi.input'];
    del environ['wsgi.version'];
    
    f = file('z_aux.py','wb');
    f.write('env = ' + str(environ));
    f.close();

def application(environ, start_response):
    
    #we assume we're serving an HTML
    Header.init();
    Header.set('Content-Type','text/html; charset=utf-8');
    Status.set(200);
    
    #hijack sysout; we can print content instead of returning it
    old_stdout = sys.stdout;
    output = StringIO();
    sys.stdout = output;
    app = None;
    try:
        #dump_file(environ);
        app = System.init(environ);
        app.run();
    except Exception as e:
        traceback.print_exc(file=sys.stdout);
        Status.set(500);
    finally:
        System.clear_instance();
    
    sys.stdout = old_stdout;
    content = output.getvalue() or '';
    if isinstance(content, unicode):
        content = content.encode('UTF-8');
    n = len(content);
    Header.set('Content-Length', n);
    start_response(Status.str(), Header.get());
    
    #start yielding pieces to be output to stdout
    #returning the whole string causes massive performance issues
    #string - iterable, so every character is flushed by Apache
    i = 0;
    buf_size = 128 * 1024;
    while i<n:
        yield content[i:i + buf_size];
        i = i + buf_size;
