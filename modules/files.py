import config.paths;
import os.path;
import re;
import string;
import codecs;
import mimetypes;
import magic;
import sys;

import config.params;
import jupp;
from Language import Language;
from Header import Header;
from Status import Status;

_RAW = 0;
_REPL_LANG = 1;
_REPL_CONST = 2;

def get(*args):
    
    ftype = args[0];
    fname = args[1];
    if (ftype not in ['js','css', 'img']):
        raise ValueError('Invalid file type');
    if './' in fname or '.\\' in fname:
        raise ValueError('Invalid file name');
    fpath = config.params.syspath + getattr(config.paths, ftype) + fname;
    if not os.path.isfile(fpath):
        Status.set(404);
        raise ValueError('File does not exist');
    ext = '.min.'+ftype;
    min = True if fname[-len(ext):].lower() == ext else False;
    
    if ftype == 'js':
        Header.set('Content-Type', 'application/javascript; charset=utf-8');
        mode = _REPL_LANG;
    elif ftype == 'css':
        Header.set('Content-Type', 'text/css; charset=utf-8');
        mode = _REPL_CONST;
    else:
        with magic.Magic(flags=magic.MAGIC_MIME_TYPE) as m:
            Header.set('Content-Type', m.id_filename(fpath));
        Header.set('Content-Disposition', 'attachment; filename="%s"' % fname);
        mode = _RAW;
    
    _out_file(fpath, ftype, mode);


def _out_file(fpath, ftype, mode = _RAW):
    try:
        #keep in mind, output is a raw string. But then again, we're not really that interested since we're replacing only ascii names
        f = open(fpath, 'rb');
        if mode != _RAW:
            output = f.read();
            if mode & _REPL_LANG:
                output = re.sub('{\$[A-Z_\-\.]+}', _repl_lang, output);
            if mode & _REPL_CONST:      
                output = re.sub('{\$[A-Z_\-\.]+}', _repl_const, output);
            sys.stdout.write(output);
        else:
            while 1:
                tmp = f.read(128 * 1024);
                sys.stdout.write(tmp);
                if not len(tmp):
                    break;
        f.close();
    except Exception:
        Status.set(500);
        raise Exception("Could not read file "+fname);
    
def _repl_lang(match):
    s = match.group(0);
    s = s.translate(None, '${}');
    return Language.translate(s);

def _repl_const(match):
    s = match.group(0);
    s = s.translate(None, '${}');
    
    if (s == 'IMG_DIR'):
        return paths.img;
    elif s == 'CSS_DIR':
        return paths.css;
    else:
        return s;