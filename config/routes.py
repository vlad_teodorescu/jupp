from Router import Router;

Router.add(r'^/files/get/(.+?)/(.+)/$', r'files:get');
Router.add(r'^/index/([^/]+)/$', r'index:\1');
Router.add(r'^/$', r'index:index');