function ajax(url,callback)
{
	if (!isset(url) || !url.length)
		return false;
	$.ajax({
			url: homeURL+url,
			type:"POST",
			success:callback
			});
		return true;
}

function create_dataTable(opts)
{
	var defaults =
	{
		//table: null,
		columns: null,
		//module: null,
		//url: null,
		bServerSide : true,
		'edit': '/edit/',
		'delete': '/delete/',
		'paginate': '/paginate/'
	};
	
	opts = $.extend(defaults, opts);
	
	if (opts.bServerSide)
	{
		if (typeof opts.url !== 'undefined' && opts.url !== null)
			src_url = opts.url;
		else
			src_url = HOME + opts.module + opts['paginate'];
	}
	else
		src_url = null;
	
	oTable = $(opts.table).dataTable({
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"bProcessing": true,
			"bServerSide": opts.bServerSide,
			"aoColumns": opts.columns,
			"sAjaxSource": src_url,
			"oLanguage": dataTables_lang,
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				var table = this;
				$.ajax( {
					"type": "POST",
					"loadingImage": false,
					"url": sSource, 
					"data": aoData, 
					"success": 	function(data) {fnCallback(data);uibuttons(opts.table)},
					"error": function() {table.fnProcessingIndicator(false);}
				});
			}			
		});
	
	oTable.deleteRow = function(id, nume)
	{
		id = Base64.decode(id);
		nume = Base64.decode(nume);
		styled_confirm(
			{
				title:'{$CONFIRM}?',
				message:'{$QUESTION_CONFIRM_REMOVE}'.replace("$1", nume)				
			},
			function() { ajax( opts.module + opts['delete'] + id, function(data) {notice(data,null); oTable.fnDraw(); }) }
		);
	}
	
	oTable.editRow = function(addr)
	{
		addr = Base64.decode(addr);
		location.href = HOME + opts.module + opts['edit'] + addr;
	}
	
	oTable.fnSetFilteringDelay(500);
	return oTable;
}
