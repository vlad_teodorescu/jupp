import sys;
import os;
from StringIO import StringIO;

#os.chdir(os.path.dirname(__file__));
sys.path.append(os.getcwd());
sys.path.append(os.getcwd()+'/system');

import jupp;
from System import System;
from Status import Status;
from Header import Header;
import config.params;

def application(environ):
    
    #hijack sysout; we can print content instead of returning it
    old_stdout = sys.stdout;
    output = StringIO();
    sys.stdout = output;
    Header.init();
    
    config.params.caller = 'cli';
    
    #environ['CONTENT_LENGTH'] = os.stat('z_input.txt').st_size;
    environ['wsgi.input'] = open('z_input.txt', 'rb');
    app = None;
    try:
        del environ['HTTP_X_REQUESTED_WITH'];
    except Exception as e:
        pass;
    
    try:
        app = System.init(environ);
        app.run();
    finally:
        sys.stdout = old_stdout;
        environ['wsgi.input'].close();
        del app;
    
    sys.stdout = old_stdout;
    content = output.getvalue() or '';
    if len(content):
        print content[0:1000];
    

namespace = {};
execfile('z_aux.py', namespace);
application(namespace['env']);

del sys.path[-2:];