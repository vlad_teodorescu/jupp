if (typeof('isset')!='function')//so we havent declared the function isset
{
	function isset(obj)
	{
		if (typeof(obj)=='undefined')
			return false;
		if (obj==null)
			return false;
		return true;
	}
}
if (typeof('String.prototype.trim')!='function')
{
	String.prototype.trim = function()
	{
		return this.replace(/^\s+|\s+$/g,"");
	}
}

function morph_phone(tel)
{
	var i=0, expr = [/^\+407\d{8}$/,/^[0]{0,1}7\d{8}$/,/^0[23]\d{8}$/];
	
	for (i=0;i<expr.length;i++)
		if (expr[i].test(tel))
			return true;
	return false;
}

function morph_registration_number(reg)
{
	if (reg.length<4)//yeah, numbers with less than 4 characters, sure.
		return false;
	var i=0;
	var exprs = [
				/^[a-z]{2}\-[\d]{2}\-[a-pr-z]{3}$/i,//match a permanent number (not Bucharest), eg GR-03-UNU. Note, the letter Q is excluded from the 3-letter group
				/^B\-[\d]{2,3}\-[a-pr-z]{3}$/i,//match a permanent number(Bucharest), eg B-664-KKO. Note, the letter Q is excluded from the 3-letter group
				/^[a-z]{1,2}[\d]{3,6}$/i//try to match a temporary number, eg AG045546
				];
	for (i=0;i<exprs.length;i++)
		if (exprs[i].test(reg))
			return true;
	return false;
}

function morph_cnp(k)
{
	var cnp = new Array();
	for (i = 0; i <=12; i++)
	{
	 cnp[i] = k[i];
	}
	suma = cnp[0] * 2 + cnp[1] * 7 + cnp[2] * 9 + cnp[3] * 1 + cnp[4] * 4 + cnp[5] * 6 + cnp[6] * 3 + cnp[7] * 5 + cnp[8] * 8 + cnp[9] * 2 + cnp[10] * 7 + cnp[11] * 9; 
	rest = suma % 11;  
	if ((rest < 10 && rest == cnp[12]) || (rest == 10 && cnp[12]==1)) 
	 validare = true;
	else
	 validare = false;
	return validare;
}

function morph_ci_serie(k)
{
	if (k.length!=2)
		return false;
	return true;
}
function morph_ci_numar(k)
{
	if (k.length!=6)
		return false;
	for (i=0;i<k.length;i++)
		if (k[i]<'0' && k[i]>'9')
			return false;
	return true;
}
function morph_cui(cui)
{
	//return true;//fuck that,we'll edit this out later
	var nr = cui.length, total = 0
	if (nr < 2 || nr > 10) {return false}
	pondere = "7532175321".substr(10-nr,nr)
	for (x=0; x < nr -1 ; ++x) {total += cui.charAt(x) * pondere.charAt(x)}
	return (cui.substr(nr-1,1) == ((total * 10) % 11 ) % 10 )	
}

function morph_email(email)
{
	var matches = email.match(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/i);	
	if (!isset(matches))
		return false;
	return (matches.length==1);
}

function morph_url(url)
{
	var matches = url.match(/^(https?:\/\/)?(www\.)?((?!www)[a-z0-9][a-z0-9.-]*\.[a-z]{2,4})$/i);
	if (!isset(matches))
		return false;
	return (matches.length>=1);
}

function morph_age(age)
{
	age = parseInt(age);
	if (isNaN(age) || age<1)
		return false;
	if (age>130)
		return false;
	return true;
}

function morph_date(date)
{
	var matches = date.match(/^[0-9]{1,2}\-[0-9]{1,2}\-[0-9]{2,4}$/);
	if (!isset(matches))
		return false;
	if (matches.length!=1)
		return false;
	var spl = date.split('-');
	var day = parseInt(spl[0]);
	var month = parseInt(spl[1]);
	var year = parseInt(spl[2]);
	if (isNaN(day) || isNaN(month) || isNaN(year))
		return false;
	if (year<100)
		year = 1900+year;
	if (day<1 || day>31)
		return false;
	if (month<1 || month>12)
		return false;
	if (year<1900)
		return false;
	var d = new Date();	
	if (year>d.getFullYear())
		return false;
	return true;
}

function validate_element(element)
{
	var value_type;
	var value;
	var tag_name = element[0].tagName;
	
	if (isset(tag_name))
		tag_name = tag_name.toLowerCase();
	else
	{
		return 4;//depending element doesn't exist
	}	
	//first, we need to check if the refered element is an input, or a div
	if (tag_name=='div' || tag_name=='fieldset')//so we have a div/fieldset, we need to scan it for the value
	{//thanks to nice naming, we can get to the input elements pretty easy			
		if (element.find('input[type=radio], input[type=checkbox]').length)//it's a buttonset/fieldset full of inputs
			value = $('#'+element.attr('id')+' input:checked').val();			
		
	}
	else//or else, it's simple stuff
	{
		value = element.val();
		if (tag_name=='input')
		{
			if ((element.attr('type')=='checkbox' || element.attr('type')=='radio') && !element.is(':checked'))//if we don't have the checked attribute on it, don't acknowledge
				value=null;//the result as being valid
		}
	}
	if (!isset(value))
		return 1;
	if (element.attr('xsd-type'))			
		value_type = element.attr('xsd-type');
	else
		value_type = 'string';			
	value_type = value_type.toLowerCase();	
	
	switch(value_type)
	{
		case 'number':
			value = parseInt(value);
			if (!isset(value) || isNaN(value) || value<=0)
				return 1;
		break;
		case 'float':					
			value = Number(value);
			if (!isset(value) || value<=0)
				return 1;
			//element.attr('xsd-allow','digits|.');//we force the xsd-allow attribute to digits|., since digits are the only thing we should be interested in
		break;
		
		case 'string':
		default:				
			value = String(value);		
			if (!isset(value) || !value.length)
				return 1;	
		break;
	}//ok, that was validation against value type
			
	if (element.attr('xsd-length'))//this is validation against length
	{
		if (String(value).length!=parseInt(element.attr('xsd-length')))
		{
			return 2;
			
		}
	}	
	if (element.attr('xsd-allow'))//watch this, it should be pretty good | validation against allowed/disallowed characters
	{
		var allowed_array = Array();//holds the allowed elements
		var v = element.attr('xsd-allow').split('|');//split against an unfamiliar character, like vertical bar, so we can allow spaces and other stuff in
		
		for (i=0;i<v.length;i++)
		{
			switch(v[i].toLowerCase())//in case somebody tries to validate against retarded stuff like DiGIts
			{
				case 'digits':
					for (j=0;j<10;j++)
						allowed_array.push(j);
				break;
				
				case 'alphanumeric':
					for (j=0;j<10;j++)
						allowed_array.push(j);
				case 'letters':
					for (j=asc('a');j<=asc('z');j++)
						allowed_array.push(chr(j));
					for (j=asc('A');j<=asc('Z');j++)
						allowed_array.push(chr(j));
				break;
				
				default:
					allowed_array.push(v[i]);
				break;
			}
		}			
		value = String(value);//cast value to string, we don't need it anymore anyway
		for (i=0;i<value.length;i++)
		{
			var ch = value.charAt(i);
			found=false;
			for (j=0;j<allowed_array.length;j++)
			{
				if ( String(ch)==String(allowed_array[j]) )//found it
				{
					found=true;
				}
			}
			if (!found)
				return 3;
		}
		
	}
	if (element.attr('xsd-morph'))//this is validation against a certain shape(or morph); 
	{//it calls the function morph_{xsd-morph}(value) which must return true(for valid shape) or false otherwise; more morphs can be added by adding more functions
	
		eval( 'var morph_ok = morph_'+element.attr('xsd-morph')+"('"+element.val()+"');" );
		if (!morph_ok)
			return 4;
	}
	return 0;//unix-style return code for OK, we're good
}

function validate_form(containerId, breakOnFirstError)//containerId is the id of the container which holds the elements, can be anything, from a form to a div
{		
	var validated = true;
	if (typeof breakOnFirstError=='undefined' || breakOnFirstError===null)
		breakOnFirstError = false;
	else
		breakOnFirstError = Boolean(breakOnFirstError);
	
	var msg = '';
	
	var container = $('#' + containerId);
	
	container.find('label.required').each(function()
	{			
		if (!isset($(this).attr('for')) || !$(this).attr('for').length)
			return true;
		var element = $('#'+$(this).attr('for')).filter('select.styled-select, :visible');//we get the element refered in the for attribute of the label, and we make sure the element is visible,
		//we also select "select"s, which is a trick bypassing styled select's display none on the selects themselves
		//makes no sense trying to validate a hidden element. If the user couldn't see it to fill it with data, why would we validate it?
		
		if (!element.length)//so if the element doesn't exist, ignore it
			return true;

		var skip_validation=false;
		if (isset($(this).attr('xsd-skip')))
			skip_validation = true;
		
		if (!skip_validation && $(this).attr('xsd-depends'))
		{			
			var depends = $(this).attr('xsd-depends');
			var negated = false;
			if (depends.trim().indexOf('!')==0)
			{
				negated= true;
				depends = depends.substr(1);
			}
			
			if ( !validate_element($('#'+ depends)) )			
				skip_validation = negated;
			else
				skip_validation = !negated;			
		}
		
		
		
		if (!skip_validation)
		{
			code = validate_element(element);//linux-style: 0 means success, others mean like following:
			if (code>0)
			{
				var str = $('label[for="'+element.attr('id')+'"]', container);
				if (str.find('.label_text').length)
					str = str.find('.label_text').html();
				else
					str = str.html();
				
				if (str.indexOf('<')!=-1)//get the text from char 0 until we find the first opening tag brace
					str = str.substr(0,str.indexOf('<'));//so we trim the string to the textnode only(cutting out the span)
					
				str = str.replace(':','').replace('_',' ').toLowerCase();
				if (msg.length)
					msg+='<br />';
				switch(code)
				{
					case 1://form field empty
						msg+='{$ERROR_VALIDATION_FIELD_EMPTY}'.replace('$1', str);
					break;
					
					case 2://form field doesn't have the required length(for fixed-length values)
						msg+='{$ERROR_VALIDATION_FIELD_WRONG_LENGTH}'.replace('$1', str);
						//'Campul "'+str+'" nu are lungimea corespunzatoare';
					break;
					
					case 3://form field has disallowed characters
						msg+='{$ERROR_VALIDATION_FIELD_INVALID_CHARACTERS}'.replace('$1',str);
						//Campul "'+str+'" contine caractere invalide';
					break;
					
					case 4://form field doesn't have the shape we wanted it to have
						msg+='{$ERROR_VALIDATION_FIELD_COMPLETED_FALSE}'.replace('$1',str);
						// Campul "'+str+'" nu este completat corespunzator';
					break;
				}
				validated=false;
				if (typeof _add_highlight_handler == 'function')
					_add_highlight_handler('error', element.attr('id'));
				//if we break on first error, then error and stop the iteration
				if (breakOnFirstError)
					return false;
			}
		}
		return true;
	});
	
	if (!validated)
		error(msg);
	
	return validated;//prevents the submit

}
