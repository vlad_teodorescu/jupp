(function($)
{
	var _options = {};
	var _container = {};
	var _object={};
	
	jQuery.fn.selectstyler = function()
    {
        return this.each(function()
        {
        	_object=$(this);
            _container = $(this).parent();
            styleHtmlSelect();
        });
        
    };
    
    function styleHtmlSelect(){
    	
    	_object.comboboxstyler()
    }
    
    $.widget("ui.comboboxstyler", {
		_create: function()
		{
			var self = this;
			var select = this.element.hide(); 
			var	obj=
				{
					source: function(request, response)
					{
						var matcher = new RegExp(request.term, "i");
						response(select.children("option").map(function()
						{
							var text = $(this).text();
							if (this.value && (!request.term || matcher.test(text)))
								return {
									id: this.value,
									label: text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "$1"),
									value: text,
									noclick: isset($(this).attr('noclick'))?true:false,
									separator: isset($(this).attr('separator'))?true:false
								};
							return null;
						}));
					},
					delay: 2500,
					mustMatch: true,
					change: function(event, ui) {						
					},
					select:function(event,ui)
					{
						if (!ui.item) {
							// remove invalid value, as it didn't match anything
							select.val("0").change();
							$(this).val("");
							return false;
						}
						if (ui.item.noclick || ui.item.separator)
							return false;
						select.val(ui.item.id).change();
						self._trigger("selected", event, {
							item: select.find("[value='" + ui.item.id + "']")
						});
						return true;
					},					
					minLength: 0					
				}
			//input-ul 
			var input = $("<input>")			
				.val(_object.children("option:selected").text())
				.insertAfter(select)
				.attr("readonly","readonly")
				.attr("disabled","disabled")
				.autocomplete(obj)				
				.addClass("ui-widget ui-widget-content ui-corner-left")				
				.focusout(function(){
					//if($(this).val()=="")
						//$(this).val(_object.children("option:selected").text())
				});
			if (select.attr('width'))
				input.css('width',select.attr('width')+'px');
			input.data("autocomplete")._renderItem = function( ul, item )
			{					
				s='';
				if (item.separator)
				{
					item.noclick=true;
					s='<a onclick=""><hr /></a>';
				}
				else
					s=item.label;				
				if (!item.noclick)
					s = "<a>"+s+"</a>";
				
					return $( "<li></li>" )
					.data( "item.autocomplete", item )
					.append(s)
					.appendTo( ul );
			};
			//asta e butonul de drop-down
			$("<button class=\"combobox\" type=\"button\">&nbsp;</button>")
			.attr("tabIndex", -1)
			.attr("title", "Arata toate optiunile")
			.insertAfter(input)
			.button({
				icons: {
					primary: "ui-icon-triangle-1-s"
				},
				text: false
			}).removeClass("ui-corner-all")
			.addClass("ui-corner-right ui-button-icon")
			.click(function()
			{
				// close if already visible
				if (input.autocomplete("widget").is(":visible"))
				{
					input.autocomplete("close");
					return true;
				}
				$('ul.ui-autocomplete').filter('[role="listbox"]').css('display','none');
				// pass empty string as value to search for, displaying all results
				input.autocomplete("search", "");
				input.focus();
				return false;
			});
			select[0].target=input;
		}
	});
	
})(jQuery);