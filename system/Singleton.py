#implements functionality required by the Singleton pattern
#needs to be redone(??)

class Singleton:
    
    _instance = None;
    
    def __init__(self):
        raise Error("Singleton class cannot be instantiated");
    
    @classmethod
    def get_instance(cls):
        if cls._instance is None:
            raise Exception("Singleton %s is not instantiated" % cls.__name__);
        return cls._instance;
    
    @classmethod
    def set_instance(cls, obj):
        if not isinstance(obj, cls):
            raise TypeError("Object is not an instance of the %s class" % cls.__name__);
        if cls._instance is not None:
            raise Exception("Only one %s singleton can be instantiated" % cls.__name__);
        cls._instance = obj;
    
    @classmethod
    def clear_instance(cls):
        if cls._instance:
            del cls._instance;
            cls._instance = None;
    
    @classmethod
    def init(cls, *args, **kwargs):
        inst = cls(*args, **kwargs);
        cls.set_instance(inst);
        
        return inst;