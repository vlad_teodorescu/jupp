class Status:
    _code = None;
    _title = None;
    _codes = [  (100,'Continue'),(101,'Switching protocols'),
                (200,'OK'),(201,'Created'),(202,'Accepted'),(203,'Nonauthoritative information'),(204,'No content'),(205,'Reset content'),(206,'Partial content'),
                (301,'Moved permanently'),(302,'Object moved'),(304,'Not modified'),(307,'Temporary redirect'),
                (400,'Bad request'),(401,'Access denied'),(403,'Forbidden'),(404,'Not Found'),(405,'Method Not Allowed'),(406,'Client browser does not accept the MIME type of the requested page'),(408,'Request timed out'),(412,'Precondition failed'),
                (500,'Internal server error'),(501,'Header values specifies a configuration that is not implemented'),(502,'Web server received an invalid response while acting as a gateway or proxy'),(503,'Service unavailable')
            ];
    
    @staticmethod
    def str():
        return str(Status._code)+ ' ' + Status._title;
        
    @staticmethod   
    def get_code():
        return Status._code;
    
    @staticmethod
    def set(val):
        if (not isinstance(val, int)):
            raise TypeError("Invalid status code");
        for (c,t) in Status._codes:
            if (c == val):
                (Status._code, Status._title) = (c,t);
                break;