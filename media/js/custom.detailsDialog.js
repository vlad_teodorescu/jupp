//------------------------------------------------------------------- This is a batch of functions to help us nicely display policies -------------------------------------------
function parseDialogData(item)
{
	item.find('button.buton_detalii').each(function()
	{
		var id = parseInt($(this).attr('id'));		
		if (!id)
			return null;
		
		if ($(this).hasClass('detalii_companie'))//so we have a policy details button
		{			
			$(this).click(function()
			{				
				item.dialog('close');
				var nume = $(this).attr('nume');
				companyDetails(id, nume);				
			});
		}
		else
			if ($(this).hasClass('detalii_intrebare'))
			{			
				$(this).click(function()
				{
					item.dialog('close');
					questionDetails(id);
				});
			}
			else
			{
				if ($(this).hasClass('detalii_chestionar'))
				{
					$(this).click(function()
					{
						item.dialog('close');
						quizDetails(id);
					});
				}
				else
				{
					if ($(this).hasClass('detalii_utilizator'))
					{
						$(this).click(function()
						{
							item.dialog('close');
							userDetails(id);
						});
					}
				}
			}
		return true;
	});

}

function site_details(idSite)
{
	idSite = parseInt(idSite);
	if (isNaN(idSite) || idSite<0)
		idSite = 0;
	if (!idSite)
		return false;
	$.ajax({
		type: "GET",
		url: homeURL+'items/siteDetails/'+idSite,
		success: function(data)
				{
					firstSetup('site','Site details');
					setupWindow(data,'site');
				}
	});
}

function item_details(idItem, idSite)
{
	idItem = parseInt(idItem);
	if (isNaN(idItem) || idItem<0)
		idItem = 0;
	if (!idItem)
		return false;
	idSite = parseInt(idSite);
	if (isNaN(idSite) || idSite<0)
		idSite = 0;
	if (!idSite)
		return false;
	$.ajax({
		type: "GET",
		url: homeURL+'items/itemDetails/'+idItem+'/'+idSite,
		success: function(data)
				{
					firstSetup('item','Item details');
					setupWindow(data,'item');
				}
	});
}

function userDetails(id, nume)
{
	id = parseInt(id);
	if (!id)
		return false;	
	$.ajax({
		type: "POST",
		url: homeURL+'utilizatori/get/'+id,
		success: function(data)
				{
					firstSetup('utilizator','Detalii '+nume);
					setupWindow(data,'utilizator');
				}
	});
}