#paths for media files
media = 'media/';
img = media + 'img/';
css = media + 'css/';
js = media + 'js/';
swf = media + 'swf/';
themes = media + 'themes/';

modules = 'modules/';
config = 'config/';
templates = 'templates/';
system = 'system/';
