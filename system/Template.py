import evoque.template;
import evoque.domain;
import evoque.collection;

from helpers.general import *;
import config.params;
import config.paths;

class Template:
    
    def __init__(self, name, module = None):
        
        module = module if module else config.params.module;
        if not name:
            raise Exception("No template name provided");
        
        self._d = evoque.domain.Domain(default_dir = config.params.syspath + config.paths.templates, errors = 4);
        self._t = self._d.get_template(module + '/' + name);
        self._vars = dict();
        
    
    @static_or_instance
    def assign(self, key, value = None):
        if not self:
            self = Template.get_instance();
        
        if not key:
            raise ValueError("Variable name not provided");
        
        if isinstance(key, tuple):
            self.assign(key[0], key[1]);
        elif isinstance(key, list):
            map(lambda x: self.assign(x), key);
        elif isinstance(key, dict):
            map(lambda x: self.assign(x), key.items());
        else:
            self._vars[key] = value;
    
    @staticmethod
    def get_constants():
        return dict({
            'HOME': config.params.home,
            'SELF': config.params.self
        });
    
    @staticmethod
    def init(name, module = None):
        t = Template(name, module);
        Template.set_instance(t);
    
    @static_or_instance
    def display(self):
        if not self:
            self = Template.get_instance();
        
        local_vars = dict(Template.get_constants().items() + self._vars.items());
        print self._t.evoque(locals = local_vars);
