import re;

class Router:

    #array of touples of form (pattern, callback path, action)
    _routes = [];
    
    @staticmethod
    def add(route, callback, split=None):
        if not callback.count(':'):
            raise ValueError('%s : Callback has no action' % callback);
        elif callback.count(':')>1:
            raise ValueError('%s : Callback cannot contain more than 1 action' % callback);
        if split is not None and not (isinstance(split, basestring) or isinstance(split, list)):
            raise TypeError("Unknown split parameter type");
        if (route[0:2] != "^/"):
            raise ValueError("Route %s must be anchored at the beginning of the query string (must start with ^/ )" % route);
        
        aux = callback.split(':');
        
        # 0 - route e.g. files/(.+)
        # 1 - module e.g. admin/files
        # 2 - action e.g. get
        Router._routes.append( (route, aux[0], aux[1], split) );
        
    @staticmethod
    def match(query):
        for r in Router._routes:
            #check whether the query matches the respective pattern
            match = re.search(r[0], query);
            if not match:
                continue;
            
            #create the callback by populating the groups in r[1] and r[2] with the respective values
            callback = r[1]+':'+r[2];
            callback_expanded = match.expand(callback);
            
            #get module and action by splitting the expanded callback by the defined delimiter
            (module, action) = tuple(callback_expanded.split(':'));
            
            #figure out which groups are mentioned in the callback, eliminate them
            match_callback = re.findall(r'(?<!\\)(?:\\\\)*\\(?:g<(\d+)>|(\d+))', callback);
            
            #remove the specified groups from the action parameters
            match_callback = map(lambda x: int(x[1]), match_callback);
            remainder = query[match.end(0):];
            #get the match groups and transform the list into a (i+1, match) tuple to be used later with lambda functions
            aux = match.groups();
            aux = [(i+1, aux[i]) for i in range(0, len(aux))];
            
            #filter against the groups present in match_callback, removing them
            action_params = filter(lambda x: x[0] not in match_callback, aux);
            
            #remove the index from each param, turning each element from (n, param) to param
            action_params = map(lambda x: x[1], action_params);
            
            #split the remainding query string based on the delimiters provided(if not None), then add them to the parameter list
            #useful for variable length queries, in order not to create the same route a million times
            if isinstance(r[3], basestring):
                action_params += filter(lambda x: len(x) > 0, remainder.split(r[3]));
            elif isinstance(r[3], list):
                split_pattern = '|'.join(map(lambda x: re.escape(x), r[3]));
                action_params +=  filter(lambda x: len(x) > 0, re.split('(?:'+split_pattern+')', remainder));
            
            return (module.strip('/'), action.strip('/'), action_params);
            
        raise ValueError("No action defined for %s" % query);