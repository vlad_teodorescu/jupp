function asc(str){str = String(str);return str.charCodeAt(0);}	
function chr(AsciiNum){return String.fromCharCode(AsciiNum);}

function unloadPage(x)
{		
	$('#col_300').css('display','none');
	$('#col_675').css('display','none');
	$('#page_loader').css('display','block');
}

function loadPage()
{		
	$('#page_loader').css('display','none');
	$('#col_300').css('display','none').css('visibility','visible');
	$('#col_300').fadeIn(200);
	$('#col_675').fadeIn(200);
	//$('#col_300').css('display','block');
	//$('#col_675').css('display','block');
}

function hide(element)
{
	$('#'+element+'_container').fadeOut(speed);
	$('label[for="'+element+'"]').attr('xsd-skip','xsd-skip');
}
function unhide(element)
{
	$('#'+element+'_container').fadeIn(speed);
	$('label[for="'+element+'"]').removeAttr('xsd-skip');
}

function question(data,callback)
{
	var title = $(data).children('title').html();
	var message = $(data).children('message').html();	
	var buttons={};
	var i=0;
	if (!isset(callback))
		callback=styled_alert;
	
	$(data).children('answers').children('answer').each(function()
	{
		var action = $(this).attr('action');
		buttons[$(this).attr('label')] = function() { $(this).dialog('close');ajax(action,callback);};		
	});
	
	$("#question-details").detach();
	$("body").append('<div id="question-details" title="'+title+'">'+message+'</div>');
	
	$('#question-details').dialog(
	{
		width:500,
		height:200,					
		modal: true,
		buttons: buttons
	});

}


String.prototype.ucfirst = function()
{
    return this.charAt(0).toUpperCase() + this.slice(1);
}
String.prototype.trim = function()
{
	return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() 
{
	return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() 
{
	return this.replace(/\s+$/,"");
}
Date.prototype.addDays = function(days) 
{
	this.setDate(this.getDate()+days);
}
Date.prototype.subDays = function(days) 
{
	this.setDate(this.getDate()-days);
}
Array.prototype.implode = function(glue) 
{
  
    var i = '', retVal='', tGlue='';
	pieces = this;
    if (arguments.length === 0) 
	{	
        glue = '';
    }
    if (typeof(pieces) === 'object') 
	{
        if (pieces instanceof Array)
			return pieces.join(glue);        
        else 
		{
            for (i in pieces) 
			{
                retVal += tGlue + pieces[i];
				tGlue = glue;
            }
            return retVal;
        }
    }    
	else 	
        return pieces;    
}

Number.prototype.toDecimals = function(k)
{
	k = parseInt(k);
	var ret,ret2;
	if (k<0)
		return '';
	
	if (k==0)
		return this.toFixed(0);
	
	ret = this.toFixed(k);
	do
	{
		k--;
		if (k<0)
			break;
		if (k==0)		
			ret2 = Number(ret).toFixed(0)+'.'+'0';
		else
			ret2 = Number(ret).toFixed(k)+'0';		
		if (ret!=ret2)
		{
			k++;
			break;
		}
		else
			ret=this.toFixed(k);
	} while (k>0);
	
	ret = this.toFixed(k);
	return ret;
}

function isArray(obj) 
{
    return obj.constructor == Array;
}

function isset(obj)
{
	if (typeof(obj)=='undefined')
		return false;
	if (obj==null)
		return false;
	return true;
}

Array.prototype.flip = function ()
{
	var newarr = new Array();
	var a = this;
	for (i in a)
	{
		if (!isset(a[i]) || typeof(a[i])=='function')
			continue;
		for (j in a[i])
		{
			if (!isset(a[i][j]) || typeof(a[i][j])=='function')
				continue;		
			if (!isset(newarr[j]) || !isArray(newarr[j]))
				newarr[j] = new Array();
			newarr[j][i] = a[i][j];
		}
	}
	return newarr;
}


function urldecode(psEncodeString)
{
  // Create a regular expression to search all +s in the string
  var lsRegExp = /\+/g;
  // Return the decoded string
  return unescape(String(psEncodeString).replace(lsRegExp, " "));
}

function changeStyledSelect(selector,value,trigger)
{
	if (!isset(value))//nothing to change
		return false;
	value = String(value);//convert it to string, so it doesn't buzz whenever we try to change to an int
	if (!isset(trigger))
		trigger=true;	
	var elem = selector;
	if (typeof(elem)=='string')
		elem = $(elem);
	elem.each(function()
	{
		$(this).find('option').removeAttr('selected');
		$(this).find('option[value="'+value+'"]').attr('selected','selected');
		//$(this).val(value).change();
		if (trigger)
			$(this).trigger('select');
		$(this).next('input').val($(this).find('option:selected').html());
		return true;
	});
	return true;
}

function changeSelect(selector,value,trigger)
{
	if (!isset(value))//nothing to change
		return false;
	value = String(value);//convert it to string, so it doesn't buzz whenever we try to change to an int
	if (!isset(trigger))
		trigger=true;	
	var elem = selector;
	if (typeof(elem)=='string')
		elem = $(elem);
	elem.each(function()
	{
		$(this).find('option').removeAttr('selected');
		$(this).find('option[value="'+value+'"]').attr('selected','selected');
		//$(this).val(value).change();
		if (trigger)
			$(this).trigger('select');
		//$(this).next('input').val($(this).find('option:selected').html());
		return true;
	});
	return true;
}

function changeRadioSet(selector,value)
{
	if (!isset(value))
		return false;
	$(selector+' input').removeAttr('checked');//deselected the radio
	$(selector+' input[value="'+value+'"]').attr('checked','checked');
	$(selector).buttonset('refresh');
	return true;
}

function reset_form_field(elem)
{
	if (typeof elem=='string')//since we need to accept elements(for the $.each below) and ID's alike, we will consider string parameters as ID's and act accordingly
		elem = $('#'+elem);
	if (!elem.length)
		return error('{$ERROR_SYSTEM}: {$ERROR_ELEMENT_NOTFOUND}');
	var reset_value = elem.attr('reset').trim();
	if (!isset(reset_value))
		return error('{$ERROR_SYSTEM}('+elem.attr('id')+')');
	
	var elem_type = elem[0].tagName.toLowerCase();
	
	//we can have one of the following: textarea, select, input of types text, password, hidden or file.
	//Note: Opera requires special treatment for resetting file fields, namely creating an identical file input and replacing the old one. This will NOT be covered here
	if (elem_type == 'input' || elem_type == 'textarea' || elem_type == 'select')
	{
		elem.val(reset_value);//that was easy
	}
	else//this is probably a container of radio buttons/checkboxes
	{
		var i=0;
		var boxes = elem.find('input[type=radio], input[type=checkbox]');
		boxes.removeAttr('checked');
		reset_value = reset_value.split('|');
		for (i=0;i<reset_value.length;i++)
			boxes.filter('[value="'+reset_value[i]+'"]').attr('checked','checked');
	}
	return true;
}

function reset_form_values(form)
{
	if (typeof form == 'string')
		form = $('#'+form);
	if (!form.length)
		return error('{$ERROR_SYSTEM}('+idForm+')');
	var children = form.find('*[reset]').not('form');
	children.each(function()
	{
		reset_form_field($(this));
	});
}

function closeWindow() 
{
	myWin = window.open('','_parent','');
	
	myWin.close();
}
function popUp(address,target)
{
	if (!isset(target) || !target.length)
		target='_blank';
	myWin = window.open(address,target,'');//'location=0, toolbar=0, status=0, menubar=0,modal=yes,alwaysRaised=yes');
}

function actions(element)
{	
	var opt = element.find('option:selected');
	var oc = opt.attr('action');	
	if (isset(oc))
	{
		changeStyledSelect(element, '0',false);
		eval(oc);		
	}
}
//image id = id-ul imaginii rezultate
//upload_target = id-ul iframe-ului rezultat
//formular = id-ul formularului
//progres_incarcare = id-ul imaginii de progress
function upload_image(id, opts, callback)//id-ul
{
	var default_opts =
	{
		image:'image_id',
		iframe:'upload_target',
		form:'formular',
		progress:'progres_incarcare',
		starter:'upload_logo',
		logo:'logo'
	}
	
	opts = $.extend(true, default_opts, opts);
	
	$('#'+opts.starter).val('1');
	if ($('#'+opts.logo).val().length)
		$('#'+opts.progress).css('visibility','visible');
	
	
	$('#'+opts.form).submit();
	$('#'+opts.iframe).unbind();
	$('#'+opts.iframe).load(function()
	{								
		$('#'+opts.progress).css('visibility','hidden');
		var data = $('#'+opts.iframe).contents().find('body').html();
		data = $(data);																
		
		if (!data.find('success').length)
			if (mode=='add')
				return error("{$ERROR_IMAGE_SAVE}");
		var image_id = data.find('id').html();
		if (image_id!='NULL')
			image_id = parseInt(image_id);								
		if (!isset(image_id) || isNaN(image_id))
			image_id='NULL';
			
		var old_img_id = parseInt($('#'+opts.image).val());								
		if (isNaN(old_img_id) || old_img_id<=0)//id invalid
			old_img_id=0;
		
		if (image_id!='NULL')//daca e null, nu trebuie scris peste, pentru ca si daca old_id e null, rezultatul e acelasi
			$('#'+opts.image).val(image_id);//id-ul imaginii de procesat
		
		if (isset(image_id) && !isNaN(image_id))
			set_thumbnail(image_id);
		$("#"+opts.starter).val('0');
		if (isset(callback) && typeof(callback)=='function')
			callback(id);
		return true;
	});
}
function getCookie(c_name)
{
	var i,x,y,ARRcookies=document.cookie.split(";");
	for (i=0;i<ARRcookies.length;i++)
	{
		x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
		y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
		x=x.replace(/^\s+|\s+$/g,"");
		if (x==c_name)
			return unescape(y);
	}
	return null;
}
function setTabCookie(tab, identifier)
{
	var index = parseInt(tab.tabs('option', 'selected'));
	identifier = 'tabs_'+identifier;
	
	var exdate=new Date();
	var exdays = 1;//1 day
	exdate.setDate(exdate.getDate() + exdays);
	var expires="; expires="+exdate.toUTCString();
	expires+="; path="+PATH+"; domain="+DOMAIN;
	var s = identifier+'='+index+expires;
	document.cookie = s;
}

function getTabCookie(identifier)
{
	var index=0;
	identifier = 'tabs_'+identifier;
	var gc = getCookie(identifier);
	gc = parseInt(gc);
	if (!isNaN(gc) && gc>=0)
		index = gc;
	return index;
}

function clearTabCookie(identifier)
{
	identifier = 'tabs_'+identifier;
	var exdate=new Date();
	var exdays = 10;//way back in time, so the cookie is expired
	exdate.setDate(exdate.getDate() - exdays);
	var expires="; expires="+exdate.toUTCString();
	expires+="; path="+PATH+"; domain="+DOMAIN;
	var s = identifier+'=0'+expires;
	document.cookie = s;
}