import jupp;
import config.tables;
from helpers.general import *;

class Language:
    
    def __init__(self, id):
        self.set(id);
        
    def set(self, id):
        item = jupp.db.get_row(config.tables.languages, where = ('id', id) );
        if not item:
            raise ValueError("Language " + id + " does not exist");
        
        self._id = item['id'];
        self._name = item['name'];
        self._load();
    
    def _load(self):
        if not self._id:
            raise Exception("No language id");
        
        db_transl = jupp.db.select(table = config.tables.translations, columns = ['id','text'], where = ('lang', self._id) );
        if not db_transl:
            db_transl = [];
        
        self._translations = dict(map(lambda x: [x['id'],x['text']], db_transl));
        
    @static_or_instance
    def translate(self, text):
        if not self:
            self = jupp.lang;
        if not text:
            return text;
        return self._translations.get(text, text);


    def _get(self, var):
        return getattribute(self, var);
    def _set(self,var, value):
        return setattribute(self, var, value);
