import sha, time, random;
import cPickle as pickle;

import jupp;

import config.params;
import config.tables;

class Session:
    
    _name = 'SSID';
    _valability = 86400; #one day
    
    def __init__(self):
        try:
            self._sid = config.params.cookie[Session._name].value;
            
            r = jupp.db.get_row(table = config.tables.sessions, where = ('id', self._sid));
            self._data = pickle.loads(r['data']) if r else dict();
        except KeyError:
            self._data = dict();
            #add a random value into the mix. Somewhat like seeding
            tmp = repr(time.time()) + str(random.randint(100,100000));
            self._sid = sha.new( tmp ).hexdigest();
            
        # New cookie containing the SID, expiring in a day
        config.params.cookie[Session._name] = self._sid
        config.params.cookie[Session._name]['expires'] = Session._valability;

    def save(self):
        data = pickle.dumps(self._data);
        jupp.db.upsert(config.tables.sessions, [('time', int(time.time())), ('data', data)], ('id', self._sid));
    
    def __del__(self):
        self.save();
    
    #load session variables from another dictionary/array/tuple
    def load(self, key, value = None):
        if isinstance(key, basestring):
            self._data[key] = value;
        elif isinstance(key, tuple):
            self._data[key[0]] = key[1];
        elif isinstance(key, list):
            map(self.set, key);
        elif isinstance(key, dict):
            self._data = dict(self._data.items() + key.items());
        else:
            raise TypeError("Session.load: Inappropriate argument type for key value ");
            
    #Implement dictionary access, I think it's cooler than get/set methods
    def __len__(self):
        return self._data.__len__();
    def __getitem__(self, key):
        return self._data.__getitem__(key);
    def __setitem__(self, key, value):
        return self._data.__setitem__(key, value);
    def __delitem__(self, key):
        return self._data.__delitem__(key);
    def __iter__(self):
        return self._data.__iter__();
    def __reversed__(self):
        return self._data.__reversed__();
    def __contains__(self, key):
        return self._data.__contains__(key);