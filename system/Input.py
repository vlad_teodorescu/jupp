import cgi;
import urlparse;
import re;
import os;

'''
form.name The name of the field, if it is specified 
form.filename If an FTP transaction, the client-side filename 
form.value The value of the field as a string 
form.file file object from which data can be read 
form.type The content type, if applicable 
form.type_options The options of the 'content-type' line of the HTTP request, returned as a dictionary 
form.disposition The field 'content-disposition'; None if unspecified 
form.disposition_options The options for 'content-disposition' 
form.headers All of the HTTP headers returned as a dictionary
'''

#Populates the get, post and files array
class Input:
    
    OK = 0;
    ERR_NO_FILE = 1;
    ERR_TMP_FILE = 2;
    
    def __init__(self, env):
        self.get = dict();
        self.post = dict();
        self.files = dict();
        
        self._re_name_index = re.compile('^([a-z0-9\-_\.]+)\s*\[\s*([a-z0-9\-_\.])*\s*\]',re.I);
        
        #take care of GET variables
        self.get = urlparse.parse_qs(env['QUERY_STRING']);
        del env['QUERY_STRING'];
        for key in self.get.keys():
            if len(self.get[key]) == 1:
                self.get[key] = self.get[key][0];
        
        #hier be only POST and FILE uploads
        fdata = cgi.FieldStorage(fp=env['wsgi.input'], environ=env);
        
        #it is EXTREMELY important that keys be sorted in this manner. Otherwise, index-less arrays could generate an index
        #which might be later overwritten by a indexed array. E.g. item[2] generated, later overwritten by item[2] provided from html
        keys = fdata.keys();
        keys.sort(reverse = False);
        
        for key in keys:
            item = fdata[key];
            
            #note: this code has the side effect of overwriting duplicates, which is exactly what I want.
            if not isinstance(item, list):
                item = [item];
            
            for list_item in item:
                container = self.files if list_item.filename is not None else self.post;
                self._save(container, key, list_item);
            
        #yop, we're done saving.
        #TODO: check if the file handle provided is actually working

    def _save(self, container, key, item):
        if not isinstance(container, dict):
            raise TypeError("Container must be a dictionary");
        try:
            key = int(key);
        except Exception:
            pass;#don't really care, at least we tried
        
        if isinstance(key, int) or (key.count('[') == 0 and key.count(']') == 0):
            if item.filename is None:
                container[key] = item.value;
            else:
                container[key] = self._createFileItem(item);
            return;
        
        #at this point we're pretty sure the key has at least one paranthesis, so there's no chance it's an in, no need for paranoia
        
        match = self._re_name_index.match(key);
        if match is None:
            raise ValueError('Malformed key: %s' % key);
        new_key = match.group(1) or '';
        new_index = match.group(2) or '';
        
        if not len(new_key):
            raise ValueError('Empty key for item: %s' % item.name);
        
        rest = key[match.span()[1]+1:];
        
        if new_key not in container or not isinstance(container[new_key], dict):
            container[new_key] = dict();
        
        #in the case of empty strings, we take PHP's pretty smart approach and populate
        #the index with the smallest available integer value
        if not len(new_index):
            i = 0;
            while i in container[new_key]:
                i = i+1;
            new_index = str(i);
        
        #ok, that's that, we finished iterating through the array
        new_index = new_index + rest;
        self._save(container[new_key], new_index, item);

    def _createFileItem(self, item):
        
        obj = {
            'name' : item.filename if len(item.filename) else None,
            'tmp_file' : item.file if isinstance(item.file, file) else None,
            'type' : item.type if len(item.filename) else None,
            'size' : os.fstat(item.file.fileno()).st_size if isinstance(item.file, file) else None
        };
        if not obj['name']:
            obj['error'] = Input.ERR_NO_FILE;
        elif not isinstance(obj['tmp_file'], file):
            obj['error'] = Input.ERR_TMP_FILE;
        else:
            obj['error'] = Input.OK;
        
        return obj;