function popup_dialog(opts)
{	
	if($("#dialog-confirm_54768").length == 0)
	{
		var s='';
		if (!isset(opts.align))
			opts.align='left';
		if (isset(opts.title))
			s+='<div id="dialog-confirm_54768" title="'+opts.title+'" style="text-align:'+opts.align+'">';
		else
			s+='<div id="dialog-confirm_54768">';
		
		s+='<span style="font-size:12px">';		
		if (isset(opts.icon))
			s+='<span class="ui-icon '+opts.icon+'" style="float:left;margin:0px 7px 20px 0px;"></span>';		
		if (isset(opts.message))
			s+=opts.message;
		s+='</span></div>';
		
		$("body").append(s);
	}
	$("#dialog").dialog("destroy");
	$("#dialog-confirm_54768").dialog({
										resizable: opts.resizable,
										height:opts.height,
										width: opts.width,
										modal: true,
										close: (isset(opts.onClose) && typeof(opts.onClose)=='function')?opts.onClose:null,
										buttons: {'Ok':function() {
																	$(this).dialog('close');
																	$(this).detach();
																	if (isset(opts.callback) && typeof(opts.callback)=='function')
																		opts.callback();
																	}
												}});
	return true;
}


//message - self explanatory, ffs
//source - the id of the element who triggered the error
function warning(message, source, callback)
{
	setTimeout(function(){_do_drop_down_action('warning', message, source, callback)},100);
	return false;
}

//message - self explanatory, ffs
//source - the id of the element who triggered the error
function notice(message, source, callback)
{
	setTimeout(function(){_do_drop_down_action('notice', message, source, callback)},0);
	return false;
}

//message - self explanatory, ffs
//source - the id of the element who triggered the error
function error(message, source, callback)
{
	setTimeout(function(){_do_drop_down_action('error', message, source, callback)},200);
	return false;
}

function _do_drop_down_action(name, message, source, callback)
{
	_set_container_text(name,message);
	_drop_down_container(name, callback);
	_add_highlight_handler(name, source);
}

function _get_event_type(elem)
{
	var elem_type = elem[0].tagName.toLowerCase();
	var event = 'focus';
	if (elem_type!='input' && elem_type!='select' && elem_type!='textarea')
		return 'click';
	return 'focus';
}

function _drop_down_container(name, callback)
{
	var container = _get_container_by_name(name);
	var height = _get_container_height(container);
	container.css('margin-top',(-1*height)+'px').css('display','block');
	
	//try to fake a function which does nothing in order not to error on animate
	if 	(typeof callback=='function')
	{
		var cb = function()
		{
			setTimeout(callback, 3500);	
		}
	}
	else
		var cb = function() {};
	
	if (!isset(speed))
		speed = 500;
	container.animate({
		marginTop:'0px'
	}, 'easeInBounce', cb);
}

function _add_highlight_handler(name, source)
{
	if (source===null || typeof source=='undefined' || !source.length)
		return false;
	
	var src = $('#'+source);
	src.addClass(name);
	
	var etype = _get_event_type(src);
	
	
	switch (name)
	{
		case 'error':
			src.bind(etype,_remove_highlight_handler_error);
		break;
		case 'warning':
			src.bind(etype,_remove_highlight_handler_warning);
		break;
		case 'notice':
			src.bind(etype,_remove_highlight_handler_notice);
		break;
	}
	return true;
}

function _remove_highlight_handler_error(e)
{
	$(e.currentTarget).removeClass('error').unbind(e.type, _remove_highlight_handler_error);
}

function _remove_highlight_handler_warning(e)
{
	$(e.currentTarget).removeClass('warning').unbind(e.type, _remove_highlight_handler_warning);
}

function _remove_highlight_handler_notice(e)
{
	$(e.currentTarget).removeClass('notice').unbind(e.type, _remove_highlight_handler_notice);
}

function _remove_highlight_handler_all(elem)
{
	var e = {};
	e.currentTarget = elem;
	_remove_highlight_handler_error(e);
	_remove_highlight_handler_warning(e);
	_remove_highlight_handler_notice(e);
}

function _get_container_by_name(name)
{
	return $('#dropdown_container_'+name);
}
function _set_container_text(name, text)
{
	var container = _get_container_by_name(name);
	container.append('<div class="info_item">' + text + '</div>');
}

function _get_container_height(container)
{
	var height = parseInt(container.height());//try the old fashioned way
	if (isNaN(height) || height<1)
	{
		height = parseInt(container.css('height').replace(/px/ig,''));
		if (isNaN(height) || height<1)//still didn't work
			height = 40;//fallback
	}
	return height;
}

function _clear_container_text(name)
{
	var container = _get_container_by_name(name);
	container.html('');
}

function _hide_all_containers(force)
{
	var i, containers = ['error','warning','notice'];
	if (typeof force =='undefined' || force===null)
		force = false;
	else
		force = Boolean(force);
	var execute = function(i)
	{
		if (force)
			_hide_container(containers[i], true);
		else
			setTimeout(function(){_hide_container(containers[i], false);}, i*100);
	}
	for (i=0;i<containers.length;i++)
		execute(i);
}

function _hide_container(name, force)
{
	if (force===null || typeof force=='undefined')
		force = false;
	else
		force = Boolean(force);
	
	var container = _get_container_by_name(name);
	var height = _get_container_height(container);
	if (!container.is(':visible'))//hidden somehow?
	{
		container.css({
			marginTop: (-1 * height)+'px',
			display: 'none'
		});
		_clear_container_text(name);
		return true;
	}
	if (!isset(speed))
		speed = 500;
	if (force)
	{
		container.css({
			'margin-top': (-1*height)+'px',
			'display':'none'
		});
		_clear_container_text(name);
		return true;
	}
	else
		container.animate({
						marginTop: (-1*height)+'px'
					  },
					  speed,
					  function()
						{
							container.css({
								display: 'none'
							});
							_clear_container_text(name);
							return true;
						});
	return true;
}

$(document).ready(function()
{
	_get_container_by_name('error').click(function(){_hide_all_containers()});
	_get_container_by_name('warning').click(function(){_hide_all_containers()});
	_get_container_by_name('notice').click(function(){_hide_all_containers()});
});

// This section deals with hijacking the original $.ajax function and replacing it with a custom-written one
function attachProgressImage()//attaches a "progress" image to the screen while ajax happens
{
	id = '142159';
	//return true; //Uncomment to remove progress image function	
	if ($('#ajax_container_'+id).length)
		$('#ajax_container_'+id).remove();
	
	$('body').append('<div id="ajax_container_'+id+'" class="ui-widget-overlay"></div>');
	var container = $('#ajax_container_'+id);
	//container.append('<img src="'+IMG_DIR+'loading.gif" id="loading_'+id+'" style="width:300px; height:300px; display:block; position:fixed;" />');
	var doc_height = $(document).height();	
	container.css('height',doc_height+'px');
	var img_top = ($(window).height() - 300)/2;
	var img_left = ($(window).width() - 300)/2;
	$('#loading_'+id).css('top',img_top+'px').css('left', img_left+'px');
}

function removeProgressImage()//removes the progress image
{
	id = '142159';
	//return true; //Uncomment to remove progress image function	
	$('#ajax_container_'+id).remove();
}

if (typeof($) !== 'undefined')
{
$._original_ajax = $.ajax;
$.ajax = function(url, settings)
{
	$('.error, .warning, .notice').each(function(){_remove_highlight_handler_all($(this))});
	_hide_all_containers(true);
	
	if ( typeof url === "object" ) {
		settings = url;
		url = settings.url;
	}
	delete settings.url;

	// Force settings to be an object
	settings = settings || {};
	
	defaults = 	{
					intercept: true,
					loadingImage: true,
					type: "POST"
				};
	
	settings = $.extend( defaults, settings);
	
	if (settings.intercept)
	{
		settings.dataType = "json";
		
		if (typeof(settings.success)!=='undefined')
			var orig_success = settings.success;
		
		if (typeof(settings.warning)!=='undefined')
			var orig_warning = settings.warning;
		
		if (typeof(settings.error)!=='undefined')
			var orig_error = settings.error;
		
		if (typeof(settings.complete)!=='undefined')
			var orig_complete = settings.complete;
			
			
		//TODO: modify the behaviour of warnings so that it allows for custom warning handlers
		settings.success = function(data, textStatus, jqXHR)
		{
			var i;
			data = data || {};
			
			if (typeof(data.error) === 'undefined' || data.error === null)
			{
				if (typeof(data.warning) !== 'undefined' && data.warning !== null)
					for (i = 0; i < data.warning.length; i++)
						warning(data.warning[i].msg, data.warning[i].rel);
					
				if (typeof(orig_success) === 'function')
					orig_success(data.response, textStatus, jqXHR, data.params)
				else if (typeof(orig_success) === 'array')
					for (i = 0; i < orig_success.length; i++)
						orig_success[i](data.response, textStatus, jqXHR, data.params);
			}
			else
			{
				if (typeof(data.warning) !== 'undefined' && data.warning !== null)
					for (i = 0; i < data.warning.length; i++)
						warning(data.warning[i].msg, data.warning[i].rel);
				
				settings.error(jqXHR, 'server', data.error.msg, data.error.rel);
			}
		}
		
		settings.error = function(jqXHR, textStatus, errorThrown, rel)
		{
			switch(textStatus)
			{
				case 'server':
					var i;
			
					if (typeof(orig_error) === 'function')
						if (orig_error(jqXHR, textStatus, errorThrown) === false)
							return true;
					else if (typeof(orig_error) === 'array')
						for (i = 0; i < orig_error.length; i++)
							if (orig_error[i](jqXHR, textStatus, errorThrown) === false)
								return true;
					error(errorThrown, rel);
				break;
			
				case 'timeout':
					error('{$ERROR_AJAX_TIMEOUT}');
				break;
				case 'error':
					error('{$ERROR_AJAX_ERROR}');
				break;
				case 'abort':
					error('{$ERROR_AJAX_ABORT}');
				break;
				case 'parsererror':
					error('{$ERROR_AJAX_PARSERERROR}');
				break;
				default:
					error(errorThrown);
				break;
			}
			return true;
		}
		
		settings.complete = function(jqXHR, textStatus)
		{
			var i;
			if (settings.loadingImage)
				removeProgressImage();
			if (typeof(orig_complete) === 'function')
				orig_complete(jqXHR, textStatus)
			else if (typeof(orig_complete) === 'array')
				for (i = 0; i < orig_complete.length; i++)
					orig_complete[i](jqXHR, textStatus);
		}
	}
	if (settings.loadingImage)
		attachProgressImage();
	$._original_ajax(url, settings);
};


}
else
	error('{$ERROR_INCORRECT_ORDER_JS}');
function styled_confirm(messagiu,callback)
{
	_hide_all_containers(true);
	if (typeof(messagiu)=='string')
		opts = {message:messagiu, title:'{$CONFIRM}?', icon:'ui-icon-alert', resizable:true}
	else
		opts = messagiu;
	
	if($("#dialog-confirm_54769").length == 0)
		$("body").append('<div id="dialog-confirm_54769" title="'+opts.title+'">'+
							'<span style="font-size:12px"><span class="ui-icon '+opts.icon+'" style="float:left;margin:0px 7px 20px 0px;"></span>'+opts.message+'</span></div>');
	$("#dialog").dialog("destroy");
	$("#dialog-confirm_54769").dialog(
	{
		resizable: opts.resizable,
		height:180,
		width:400,
		modal: true,
		buttons:
		{
			'{$NO}': function()
			{
				$(this).dialog('close');
				$(this).detach();
			},
			'{$YES}':function()
			{
				$(this).dialog('close');
				$(this).detach();
				if (isset(callback) && typeof(callback)=='function')
					callback();
			}
		}
	});
}
function firstSetup(which,title,w_width,w_height,callbacks,type)
{
	if (!isset(w_width))
		w_width = 700;
	if (!isset(type))
		type = 'details';
	var close_callback=null;
	var save_callback=null;
	if (isset(callbacks))
	{
		if (typeof(callbacks)=='function')//backwards compatibility, assume that if a function is passed, it should be the close callback
		{
			close_callback = callbacks;
		}
		else
		{
			if (isset(callbacks.close))
				close_callback = callbacks.close;
			if (isset(callbacks.save))
				save_callback = callbacks.save;
		}
	}
	
	var butt;
	if (type=='details')
		butt = {'{$CLOSE}':function(){$(this).dialog('close');}};
	else if (type=='save')
		butt = {'{$SAVE}':function()
				{
					if (isset(save_callback) && typeof(save_callback)=='function')
					{
						if (save_callback($('#'+which+'-details')))
							$(this).dialog('close');
					}
					else
						$(this).dialog('close');
				},'{$CANCEL}':function(){$(this).dialog('close');}};
	
	
	
	if (!isset(w_height))
		w_height = $(window).height()-100;
	$("#"+which+"-details").detach();
	$("body").append('<div id="'+which+'-details" title="'+title+'"></div>');
	$('#'+which+'-details').html('');
	$('#'+which+'-details').dialog({
		width:w_width,
		height:w_height,					
		modal: true,
		beforeClose: function()
		{
			var ok=true;
			if (isset(close_callback) && typeof(close_callback)=='function')
				ok = close_callback($('#'+which+'-details'));
			
			return ok;
		},
		close: function(event, ui)
		{
			//var gmc = $('#'+which+'-details').data('returnto');
			//empty_msg();//empty the errror/notice containers
			//$('#general_message_container').prependTo(gmc);//TODO: make prepend safer, in case the parent is not empty
		},
		open: function(event, ui)//:KLUDGE: should be focus, so as to allow for non-modal dialogs aswell in the future
		{
			//var gmc = $('#general_message_container').parent();
			//$('#'+which+'-details').data('returnto',gmc);
			//empty_msg();//empty the error/notice containers
			//$('#general_message_container').prependTo($('#'+which+'-details'));//TODO: make prepends safer
			
		},
		buttons: butt
	});
	var img_top = ($('#'+which+'-details').height() - 300)/2;
	var img_left =($('#'+which+'-details').width() - 300)/2;	
	$("#"+which+"-loading").contentLoader();
}

function setupWindow(data,which,buttons)
{
	if (!isset(buttons))
		buttons=true;
	$("#"+which+"-details").append(data);
	parseDialogData($("#"+which+"-details"));
	
	if (buttons)
		uibuttons($('#'+which+'-details'));
	return $('#'+which+"-details");
}