import os;
import sys;
import imp;
import Cookie;

#Configuration
import config.paths;
import config.routes;
import config.db;
import config.app;
import config.params;

#required classes
from Input import Input;
from Router import Router;
from Status import Status;
from Header import Header;
from Database import Database;
from Language import Language;
from Session import Session;
from Singleton import Singleton;
import jupp;

class System(Singleton):
    
    def __init__(self, env):
        if (not isinstance(env, dict)):
            raise ValueError("Invalid environment");
        
        #something happens here, there's nothing we can do about it
        self._init_config(env);
        
        try:
            jupp.db = Database(  username = config.db.username,
                            password = config.db.password,
                            database = config.db.database,
                            host     = config.db.host,
                            driver = 'pgsql'    );
            jupp.lang = Language(config.app.default_language);
            jupp.session = Session();
            jupp.input = Input(env);
        except Exception as e:
            if not hasattr(self._mod_handle, '_syserror'):
                raise e;
            if not getattr(self._mod_handle, '_syserror')(e):
                raise e;
    
    def __del__(self):
        #delete them explicitly, since Python might not have an idea in which order to call them
        #better safe than sorry
        del self.input;
        del self.lang;
        del self.session;
        for i in config.params.cookie:
            Header.set('Set-Cookie', config.params.cookie[i].OutputString());
        del self.db;

    def run(self):
        try:
            #run the specified action
            getattr(self._mod_handle, config.params.action)(*config.params.args);
        except Exception as e:
            if not hasattr(self._mod_handle, '_error'):
                raise e;
            if not getattr(self._mod_handle, '_error')(e):
                raise e;
    
    #initialize application configuration. Paths and some other minor stuff
    def _init_config(self, env):
        config.params.script = os.path.basename(env['SCRIPT_FILENAME']);
        if (not config.params.script):
            raise Exception("Could not read script name");
        
        if env.get('HTTP_X_REQUESTED_WITH', None) == 'xmlhttprequest':
            config.params.caller = 'ajax';
        
        config.params.syspath = os.path.dirname(env['SCRIPT_FILENAME'])+'/';
        if len(config.params.syspath) < 2:
            raise Exception("Could not retrieve system directory");
        
        config.params.host = env['wsgi.url_scheme'];
        config.params.host += '://' + env['HTTP_HOST'] + '/';
        config.params.home = config.params.host + os.path.dirname(env['SCRIPT_NAME'])[1:]+'/';
        config.params.self = config.params.host.rstrip('/')+ env['REQUEST_URI'];
        config.params.query = env['PATH_INFO'].rstrip('/') + '/';
        
        config.params.cookie = Cookie.SimpleCookie();
        try:
            config.params.cookie.load(env['HTTP_COOKIE']);
        except KeyError:
            pass;
        
        (config.params.module, config.params.action, config.params.args) = Router.match(config.params.query);
        self._mod_handle = imp.load_source('run_module', config.paths.modules + config.params.module + '.py');