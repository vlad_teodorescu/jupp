import jupp;

import config.tables;

#Will hold various system and project related settings, like app title etc.
class Settings:
    
    def __init__(self):
        self._changed = False;
       
        rez = jupp.db.select(table = config.tables.settings);
        self._data = dict();
        if rez:
            for i in rez:
                self._data[i['key']] = i['value'];
    
    def __del__(self):
        if self._changed:
            self.save();

    def save(self):
        #raze all the possible existing data, since I'm in no mood to do "deleted" checks
        #TODO: make it better
        for i in self._data.keys():
            if self._data[i] is not None:
                jupp.db.upsert(table = config.tables.settings, values = ('value', self._data[i]), where = ('key', i));
            else:
                jupp.db.delete(table = config.tables.settings, where = ('key', i));
    
    #Implement dictionary access, I think it's cooler than get/set methods
    def __len__(self):
        return self._data.__len__();
    def __getitem__(self, key):
        return self._data.__getitem__(key);
    def __setitem__(self, key, value):
        self._changed = True;
        self._data.__setitem__(key, value);
    def __delitem__(self, key):
        self._changed = True;
        self._data[key] = None;
    def __iter__(self):
        return self._data.__iter__();
    def __reversed__(self):
        return self._data.__reversed__();
    def __contains__(self, key):
        return self._data.__contains__(key);