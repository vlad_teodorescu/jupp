# Thanks, internets
import functools

#catches exceptions and won't propagate them!!
class static_or_instance(object):
	def __init__(self, func):
		self.func = func
	
	def __get__(self, instance, owner):
		return functools.partial(self.func, instance)