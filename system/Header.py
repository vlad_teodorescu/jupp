
class Header:
    
    @staticmethod
    def set(*args):
        if not len(args):
            return;
        
        if (isinstance(args[0], list)):
            map(lambda x: add(x), args[0]);
            return;
        
        if (len(args) == 2):
            key = args[0];
            val = args[1];
        elif args[0]:
            (key, val) = args[0];
        else:
            return;
        
        if key is None:
            raise ValueError("Header key cannot be empty");
        
        if val is not None:
            if key in Header._headers and Header._allow_duplicates(key):
                Header._headers[key].append(str(val));
            else:
                Header._headers[key] = [str(val)];
        else:
            Header.unset(key);
    
    @staticmethod
    def get():
        headers = Header._headers.items();
        
        for i in range(0, len(headers)):
            name = headers[i][0];
            value = "; ".join(headers[i][1]);
            headers[i] = (name, value);
        return headers;
    
    @staticmethod
    def unset(name):
        try:
            del Header._headers[name];
        except KeyError:
            pass;
    
    @staticmethod
    def _allow_duplicates(key):
        allowed = ['set-cookie'];
        if key.lower() in allowed:
            return True;
        return False;

    @staticmethod
    def isset(name):
        return (name in Header._headers);
        
    @staticmethod
    def init():
        Header._headers = dict();